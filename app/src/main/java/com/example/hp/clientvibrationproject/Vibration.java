package com.example.hp.clientvibrationproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Vibration extends AppCompatActivity implements View.OnClickListener {


    private TextView Vreply1text  , Vreply2text ,Vreceivingtext;
    private CheckBox Vreply1CheckBox , Vreply2CheckBox ,VreceivingCheckBox;
    private LinearLayout VreceivingCheckBoxLayout,Vreply1CheckBoxLayout,Vreply2CheckBoxLayout ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Vreply1text = (TextView) findViewById(R.id.Vreply1text);
        Vreply2text = (TextView) findViewById(R.id.Vreply2text);
        Vreceivingtext = (TextView) findViewById(R.id.Vreceivingtext);
        VreceivingCheckBox = (CheckBox) findViewById(R.id.VreceivingCheckBox);
        Vreply1CheckBox = (CheckBox) findViewById(R.id.Vreply1CheckBox);
        Vreply2CheckBox = (CheckBox) findViewById(R.id.Vreply2CheckBox);
//        VreceivingCheckBoxLayout= (LinearLayout) findViewById(R.id.VreceivingCheckBoxLayout);
//        Vreply1CheckBoxLayout= (LinearLayout) findViewById(R.id.Vreply1CheckBoxLayout);
//        Vreply2CheckBoxLayout= (LinearLayout) findViewById(R.id.Vreply2CheckBoxLayout);


        Vreceivingtext.setOnClickListener(this);
        Vreply1text.setOnClickListener(this);
        Vreply2text.setOnClickListener(this);
        VreceivingCheckBox.setOnClickListener(this);
        Vreply1CheckBox.setOnClickListener(this);
        Vreply2CheckBox.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v==VreceivingCheckBox){

            if (VreceivingCheckBox.isChecked()){
                Vreceivingtext.setText("Enabled");
            }
            else{
                Vreceivingtext.setText("Disabled");

            }
        }
        else if (v==Vreply1CheckBox){

            if (Vreply1CheckBox.isChecked()){
                Vreply1text.setText("Enabled");
            }
            else{
                Vreply1text.setText("Disabled");

            }
        }
       else  if (v==Vreply2CheckBox){

            if (Vreply2CheckBox.isChecked()){
                Vreply2text.setText("Enabled");
            }
            else{
                Vreply2text.setText("Disabled");

            }
        }

    }
}
